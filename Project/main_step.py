## @file main.py
#  This file will combine the classes MotorDriver.py, Controller.py and Encoder.py to implement a closed-loop control for the motor position.
#  By executing this file, you can choose a proportional gain factor. The code will then run a step response with the setpoint of 1000.
#  The shell will print out a column of [time, controller_position] for the step response, which can be plotted with MATLAB for example.
#
#  @author Lukas Meggle
#  @date 05/13/20
from Controller import Controller
from MotorDriver import MotorDriver
from Encoder import Encoder
import pyb
import utime

#Creating the MotorDriver
pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
tim = pyb.Timer(3, freq = 20000);
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
moe.enable()

# Creating the Encoder
pin_chA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN);
pin_chB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN);
tim = pyb.Timer(4, prescaler=1, period=65535);
enc = Encoder(pin_chA, pin_chB, tim)

#Endless while-command to run a step response as often as you like
while(True):
    kp = float(input('Please enter kp: '))
    
    #Creating the Controller.
    con = Controller(kp)

    #Setting variables to initial values
    soll = -80;
    i = 0;
    enc.set_position(0)
    x = [];
    y = [];

    #Running closed loop control.
    moe.enable()
    print('\nRunning a step response test for kp = {:}'.format(kp))
    while(i < 101):
        enc.update()
        ist = enc.get_position()
        x.append(utime.ticks_ms())
        y.append(ist)
        conout = con.update(soll, ist)
        moe.set_duty(conout)
        utime.sleep_ms(10)
        i = i + 1;
    moe.disable()

    #Printing out the List for plotting the step response and rescaling the x list (time) to a starting point at 0.
    x = [n - x[0] for n in x]
    print('\nStep response results:\n[Time in ms, Motor position]\n')
    i = 0;
    while (i < len(x)):
        print(x[i], ', ', y[i],'\n')
        i = i + 1;
 