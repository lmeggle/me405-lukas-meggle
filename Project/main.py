'''
@file       main.py
@brief      Initializing all parameters and runs the flight code.

@details    For a detailed description, see @ref page_Main "Final Project: Flight Code"

@page       page_Main Final Project: Flight Code

@details    Main flight code that runs continuously. For a detailed description, see main.py.

@section    sec_Main_script_intro Introduction
            The main script file initializes all the classes and then runs
            a loop to control the ailerons at a fixed interval.\n
            

@section    sec_Main_script_init Initializing
            This file initializes both motors and both encoders and a proportional controller
            for both of them, initializes the gyro IMU sensor, creates 3 PWM inputs (Receiver objects),
            sets the desired PID gains, creates 2 PID controllers and initializes the initial values.

@section    sec_Main_script_receiving Receiving Signals
            First, the script will read the 3 PWM values. They are all a value between 1000 and 2000.
            If those values are smaller than 100 or bigger than 3000, the values will be ignored. This is necessary
            , because testing showed that wrong values with 6 digits are sometimes received. Pitch
            and Roll signal will also be scaled to a value between -100 and 100.
            
@section    sec_Main_script_calculating Calculating the aileron position
            Depending on the mode switch, the ailerons will either be set to a value of 0 (disabled),
            they will be "mixed" for a manual mode (because a roll signal will effect both ailerons) and a
            DC-motor setpoint will be calculated, or, in the bottom position, the stabilization system
            will be executed. In that mode, the IMU acceleration for roll and
            throttle will be calculated and compared by the desired acceleration, which is the received throttle or roll value
            (-100 to 100 deg/s). The PID controllers will then calculate the setpoint value for roll and pitch, like in the manual mode.
            These values will then be mixed to calculate the setpoint of the DC-Motors
            
@section    sec_Main_script_limiting Limiting the aileron position
            The setpoint value for the DC-motors will be limited by -80 to 80 encoder counts. This is because of the physical
            limitations of spinning the motor on the wing.
            
@section    sec_Main_script_setting Setting the Ailerons
            In the last step, the Dc-Motor - Encoder control loop will be executed to actually set the aileron position.

@author Lukas Meggle
@date 06/12/20
'''
from Controller import Controller
from MotorDriver import MotorDriver
from Encoder import Encoder
from IMUDriver import i2c
from Receiver import Receiver
from PID import PID
import pyb
import utime

# Create the pin objects for the encoder object.

## @brief   GPIO Pin for Encoder left CH A
pin_chA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN);
## @brief   GPIO Pin for Encoder left CH B
pin_chB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN);
## @brief   Initializing Timer 4
tim4 = pyb.Timer(4, prescaler=1, period=65535);
## @brief   Creates Encoder left object
encL = Encoder(pin_chA, pin_chB, tim4)

# Creates another encoder object of the second motor.
## @brief   GPIO Pin for Encoder right CH A
pin_chA = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN);
## @brief   GPIO Pin for Encoder right CH B
pin_chB = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN);
## @brief   Initializing Timer 8
tim8 = pyb.Timer(8, prescaler=1, period=65535);
## @brief   Creates Encoder right object
encR = Encoder(pin_chA, pin_chB, tim8)

# Create the pin objects used for interfacing with the motor driver
## @brief   GPIO Pin for Motor left EN_A
pin_EN_A = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
## @brief   GPIO Pin for Motor left IN1_A
pin_IN1_A = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
## @brief   GPIO Pin for Motor left IN2_A
pin_IN2_A = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
# Create the timer object used for PWM generation
## @brief   Creating Timer 3
tim3 = pyb.Timer(3, freq = 20000);
# Create a motor object passing in the pins and timer
## @brief   Creates Motor A on the left
moeL = MotorDriver(pin_EN_A, pin_IN1_A, pin_IN2_A, tim3)

# Create the pin objects used for interfacing with the motor 2 driver
## @brief   GPIO Pin for Motor right EN_B
pin_EN_B = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP);
## @brief   GPIO Pin for Motor right IN1_B
pin_IN1_B = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP);
## @brief   GPIO Pin for Motor right IN2_B
pin_IN2_B = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP);
# Create the timer object used for PWM generation
## @brief   Creates Timer 5
tim5 = pyb.Timer(5, freq = 20000);
# Create a motor object passing in the pins and timer
## @brief   Creates Motor B on the right
moeR = MotorDriver(pin_EN_B, pin_IN1_B, pin_IN2_B, tim5)

# Enabling Gyro sensor
## @brief   Hex adress of the IMU
adress = 0x28;
## @brief   Integer Bus number for i2c bus
bus = 1;
## @brief   Creates i2c object of IMU
IMU = i2c(bus, adress)
IMU.enable()

#Creating Receiver objects
#Creating Mode object
## @brief   Creates Timer 2
tim2 = pyb.Timer(2, prescaler=79, period=0x0fffffff)
## @brief   Integer Channel value for the timer for the mode input
ch_mode = 3
## @brief   GPIO Pin for the mode receiver PWM inut
pinB10 = pyb.Pin(pyb.Pin.cpu.B10)
## @brief   Creating Mode PWM object
mode = Receiver(pinB10, tim2, ch_mode)
#Creating Pitch object
## @brief   Creates Timer 1
tim1 = pyb.Timer(1, prescaler=79, period=0x0fffffff)
## @brief   Integer Channel value for the timer for the pitch input
ch_pitch = 2
## @brief   GPIO Pin for the pitch receiver PWM input
pinA9 = pyb.Pin(pyb.Pin.cpu.A9)
## @brief   Creating Pitch PWM object
pitch = Receiver(pinA9, tim1, ch_pitch)
#Creating Roll object
## @brief   Integer Channel value for the timer for the roll input
ch_roll = 1
## @brief   GPIO Pin for the roll receiver PWM input
pinA8 = pyb.Pin(pyb.Pin.cpu.A8)
## @brief   Creating Roll PWM object
roll = Receiver(pinA8, tim1, ch_roll)

#Setting PID gains

#
## @brief   Proportional gain value for the pitch stabilization
P_pitch = 5

## @brief   Integral gain value for the pitch stabilization
I_pitch = 2

## @brief   Derivative gain value for the pitch stabilization
D_pitch = -1

## @brief   Proportional gain value for the roll stabilization
P_roll = 5

## @brief   Integral gain value for the roll stabilization
I_roll = 2

## @brief   Derivative gain value for the roll stabilization
D_roll = -1

## @brief   Proportional gain value for DC Motor - Encoder control loop
P_servo = 0.35
#


#Initializing Controllers
## @brief   Creating PID Controller for Pitch
PID_pitch = PID(P_pitch, I_pitch, D_pitch)
## @brief   Creating PID Controller for Roll
PID_roll = PID(P_roll, I_roll, D_roll)
## @brief   Creating Proportional Controller for the left motor
conL = Controller(P_servo)
## @brief   Creating Proportional Controller for the right motor
conR = Controller(P_servo)

#setting initial values
## @brief   Setting aileron position 0
encL.set_position(0)
## @brief   Setting aileron position 0
encR.set_position(0)
moeR.enable()
moeL.enable()
## @brief   Setting prior value of the pitch receiving value
pitch_val_prior = 0
## @brief   Setting prior value of the roll receiving value
roll_val_prior = 0
## @brief   Setting prior value of the mode receiving value
mode_val_prior = 0
## @brief   Setting prior value of the euler angle
pos_prior = [0,0,0]
## @brief   List of accelerations calculated from the euler angels
acc = [];

while(True):
    #Ignoring wrong pitch transmission values
    ## @brief   Reading the raw PWM pitch value
    pitch_val = pitch.get_position()
    if (pitch_val < 100):
        pitch_val = pitch_val_prior;
    elif (pitch_val > 3000):
        pitch_val = pitch_val_prior;
    else:
        ## @brief   Scaling PWM pitch value
        pitch_val = (pitch_val - 1500) / 5
    pitch_val_prior = pitch_val
    #Ignoring wrong roll transmission values
    ## @brief   Reading the raw PWM roll value
    roll_val = roll.get_position()
    if (roll_val < 100):
        roll_val = roll_val_prior;
    elif (roll_val > 3000):
        roll_val = roll_val_prior;
    else:
        ## @brief   Scaling PWM roll value
        roll_val = (roll_val - 1500) / 5;
    roll_val_prior = roll_val;
    #Ignoring wrong mode transmission values
    ## @brief   Reading the raw PWM mode value
    mode_val = mode.get_position()
    if (mode_val < 100):
        mode_val = mode_val_prior;
    elif (pitch_val > 3000):
        mode_val = mode_val_prior;
    mode_val_prior = mode_val
    
    #calculation of desired aileron posiiton depending on the mode
    if(mode_val < 1200):
        ## @brief   setpoint value for right motor
        sollR = 0;
        ## @brief   setpoint value for left motor
        sollL = 0;
    elif(mode_val > 1800):
        #Calculating the acceleration value of the euler angels
        ## @brief   reading current euler angels
        pos_cur = IMU.get_euler();
        ## @brief   Zip object holding list of current and prior euler angels
        zip_object = zip(pos_cur, pos_prior)
        for list1_i, list2_i in zip_object:
            ## @brief   Setting the calculated acceleration value
            acc.append(list1_i-list2_i)
        pos_prior = pos_cur;
        #
        ## @brief   Setting setpoint pitch value
        pitch_soll = pitch_val
        ## @brief   Setting setpoint roll value
        roll_soll = roll_val
        ## @brief   Setting current pitch acceleration value
        pitch_ist = acc[1]
        ## @brief   Setting current roll acceleration value
        roll_ist = acc[2]
        #
        ## @brief   Calculating the pitch value with the PID controller
        pitch_val = -PID_pitch.update(pitch_soll, pitch_ist)
        ## @brief   Calculating the roll value with the PID controller
        roll_val = PID_roll.update(roll_soll, roll_ist)
        print('Pitch PID output')
        print(pitch_val)
        print('Roll PID output')
        print(roll_val)
        print('Euler Acceleration in deg(x, y, z)')
        print(acc)
        ## @brief   Resetting the accleration list
        acc = []
        ## @brief   Mixing the pitch and roll value to get a right aileron position
        sollR = 0.5 * roll_val + -0.5 * pitch_val
        ## @brief   Mixing the pitch and roll value to get a left aileron position
        sollL = 0.5 * roll_val + 0.5 * pitch_val
    else:
        ## @brief   Mixing the pitch and roll value to get a right aileron position
        sollR = 0.5 * roll_val + -0.5 * pitch_val
        ## @brief   Mixing the pitch and roll value to get a left aileron position
        sollL = 0.5 * roll_val + 0.5 * pitch_val
        
        
    #limiting the aileron position
    if (sollR < -80):
        sollR = -80;
    elif (sollR > 80):
        sollR = 80;
    if (sollL < -80):
        sollL = -80;
    elif (sollL > 80):
        sollL = 80;
    
    #Actuating the ailerons through the encoder - dc-motor control loop
    encL.update()
    ## @brief   Reading current motor position left
    istL = encL.get_position()
    ## @brief   Calculating the controller output to set the motor duty cycle for the left
    conoutL = conL.update(sollL, istL)
    moeL.set_duty(conoutL)   
    encR.update()
    ## @brief   Reading current motor position right
    istR = encR.get_position()
    ## @brief   Calculating the controller output to set the motor duty cycle for the right
    conoutR = conR.update(sollR, istR)
    moeR.set_duty(conoutR)

    encL.update()
    encL_pos = encL.get_position();
    encR.update()
    encR_pos = encR.get_position();
    print('EncL Position:')
    print(encL_pos)
    print('EncR Position:')
    print(encR_pos)
    print("roll = %d" % (roll_val))
    print("pitch = %d" % (pitch_val))
    print("mode = %d" % (mode.get_position()))
    utime.sleep_ms(40)
    print('\n')
