'''
@file       Encoder.py
@brief      Encoder
@details    This file holds a class for commands to control and read-out the encoder.
            
@page       page_Encoder Encoder

@brief      This file holds a class for commands to control and read-out the encoder. Please see Encoder.py for the documentation.

@details

@section    sec_Encoder_ex Example usage

    pin_chA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN);
    pin_chB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN);
    tim = pyb.Timer(4, prescaler=1, period=65535);
    enc = Encoder(pin_chA, pin_chB, tim)

    while(True):
        enc.update()
        pos = enc.get_position()
        time.sleep(1)
        print ('Position 1: {:}.'.format(pos1))


@section    sec_Encoder_test Testing
            In the example code of the file, two encoders are connected and are beeing read out seperatly. One encoder was set close to underflow, one to overflow in order to see
if the under or overflow detection was working by spinning the motor a little further.
'''

import pyb
import time

## Implements methods for controlling a counter.
#
#  This class implements a encoder class for reading out, setting, and updating a counter.
#  
#  @author Lukas Meggle
#  @date 05/06/20
class Encoder:

    ## Creates a Encoder by initializing GPIO
    #  pins and setting positional variables.
    #
    #  @param chA_pin A pyb.Pin object to use as the input for channel A from the encoder.
    #  @param chB_pin A pyb.Pin object to use as the input for channel B from the encoder.
    #  @param tim A pyb.Timer object to use for counting the encoder steps. Refer to
    #  datasheet for the STM32L476RG to know which timer to use with which pair of GPIO pins.
    #  @param self.readold Sets up a variable as the starting point of motor position, used
    #  to get a delta value in get_delta().
    #  @param self.position A variable for the absolut positional count of the motor.
    def __init__(self, chA_pin, chB_pin, tim):
        self.A = chA_pin;
        self.B = chB_pin;
        self.tim = tim
        self.counter_A = self.tim.channel(1, self.tim.ENC_A, pin=self.A)
        self.counter_B = self.tim.channel(2, self.tim.ENC_B, pin=self.B)
        self.readold = self.tim.counter()
        self.position = 0;

    ## Updates the encoder's position.
    #
    #  Updates the variable self.positon with the newest value by adding the delta
    #  with the function get_delta()
    def update(self):
        self.position = self.position + self.get_delta()

    ## Gets the encoder's position.
    #
    #  Returns the absolut encoder position by the variable self.position.
    def get_position(self):
        return self.position

    ## Sets the position of the encoder.
    #
    #  @param newpos A variable which defines the value of which the position should be set to.
    def set_position(self, newpos):
        self.position = newpos;
    
    ## Gets the delta from the last reading to the current reading of the encoder count and returns the delta.
    #
    #  It is implemented that a over or underflow can be detected.
    #  @param delta The real delta of new to old count with correction if under or overflow is detected. This variable is returned by the method.
    def get_delta(self):
        new = self.tim.counter()
        old = self.readold
        diff = new - old;
        if diff < -32768:
            delta = (65535 - old) + new;
        elif diff > 32767:
            delta = -(old + 65535 - new);
        else:
            delta = diff;
        self.readold = new
        return delta
    
if __name__ == '__main__':

    # Create the pin objects for the encoder object.
    pin_chA = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN);
    pin_chB = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN);
    tim = pyb.Timer(4, prescaler=1, period=65535);
    enc1 = Encoder(pin_chA, pin_chB, tim)
    # Creates another encoder object of the second motor.
    pin_chA = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN);
    pin_chB = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN);
    tim = pyb.Timer(8, prescaler=1, period=65535);
    enc2 = Encoder(pin_chA, pin_chB, tim)
    
    #A test to read out both encoders and setting the second encoder close to overflow for testing
    enc2.set_position(65530)
    while(True):
        enc1.update()
        pos1 = enc1.get_position()
        enc2.update()
        pos2 = enc2.get_position()
        time.sleep(1)
        print ('Position 1: {:}.'.format(pos1))
        print ('Position 2: {:}.'.format(pos2))
