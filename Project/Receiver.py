## @file Receiver.py
#  This file contains the class Receiver with all the commands for initializing, updating and setting a counter
#  to read a pwm signal.
#
#  @author Lukas Meggle
#  @date 05/06/20

'''
@file       Receiver.py
@brief      Receiver PWM reading
@details    This file contains the class Receiver, which is able to read a PWM signal and output the pulse with.
            
@page       page_Receiver Reciever PWM reading

@brief      This file contains the class Receiver, which is able to read a PWM signal and output the pulse with. Please see Receiver.py for the documentation.

@details

@section    sec_Receiver_ex Example usage

    tim2 = pyb.Timer(2, prescaler=79, period=0x0fffffff)
    ch_mode = 3
    pinB10 = pyb.Pin(pyb.Pin.cpu.B10)
    mode = Receiver(pinB10, tim2, ch_mode)
    
    tim1 = pyb.Timer(1, prescaler=79, period=0x0fffffff)
    ch_pitch = 2
    pinA9 = pyb.Pin(pyb.Pin.cpu.A9)
    pitch = Receiver(pinA9, tim1, ch_pitch)
    
    ch_roll = 1
    pinA8 = pyb.Pin(pyb.Pin.cpu.A8)
    roll = Receiver(pinA8, tim1, ch_roll)
    
    while True:
        pyb.delay(200)
        print("roll = %d" % (roll.get_position()))
        print("pitch = %d" % (pitch.get_position()))
        print("mode = %d" % (mode.get_position()))

@section    sec_Receiver_test Testing
            This class was created because of the final project. Testing was succesfully done by reading in 4 PWM signals at once from a "Crossfire nano RX" Receiver.
'''

import pyb
import time

## Implements methods for reading a pwm signal.
#
#  This class implements a receiver class for reading out a pwm signal.
#  
#  @author Lukas Meggle
#  @date 12/06/20
class Receiver:

    ## Creates a Encoder by initializing GPIO
    #  pins and setting positional variables.
    #
    #  @param pin A pyb.Pin object to use as the input for the PWM signal.
    #  @param tim A pyb.Timer object to use for counting. Refer to
    #  datasheet for the STM32L476RG to know which timer to use with which pair of GPIO pins.
    #  @param ch A integer value holding the timer channel number for the corresponding GPIO pin.
    def __init__(self, pin, tim, ch):
        self.ch = ch;
        self.pin = pin;
        self.tim = tim;
        self.start = 0;
        self.width = 0;
        self.pwm = 0;
        self.pwm_in = self.tim.channel(self.ch, pyb.Timer.IC, pin=self.pin, polarity=pyb.Timer.BOTH)
        self.pwm_in.callback(self.update)


    ## Updates the PWM pulse width.
    #
    #  Updates the variable self.pwm with the newest value of the PWM pulse width.
    def update(self, tim):
        if self.pin.value():
            # Rising edge - start of the pulse
            self.start = self.pwm_in.capture()
        else:
            # Falling edge - end of the pulse
            self.width = self.pwm_in.capture() - self.start & 0x0fffffff
        self.pwm = self.width
        #if (self.width < 939):
        #    self.pwm = 0
        #elif self.width > 1914

    ## Gets the PWM value.
    #
    #  Returns the PWM pulse width.
    def get_position(self):
        return self.pwm
            
            
if __name__ == '__main__':
    
    tim2 = pyb.Timer(2, prescaler=79, period=0x0fffffff)
    ch_mode = 3
    pinB10 = pyb.Pin(pyb.Pin.cpu.B10)
    mode = Receiver(pinB10, tim2, ch_mode)
    
    tim1 = pyb.Timer(1, prescaler=79, period=0x0fffffff)
    ch_pitch = 2
    pinA9 = pyb.Pin(pyb.Pin.cpu.A9)
    pitch = Receiver(pinA9, tim1, ch_pitch)
    
    ch_roll = 1
    pinA8 = pyb.Pin(pyb.Pin.cpu.A8)
    roll = Receiver(pinA8, tim1, ch_roll)

    while True:
        pyb.delay(200)
        print("roll = %d" % (roll.get_position()))
        print("pitch = %d" % (pitch.get_position()))
        print("mode = %d" % (mode.get_position()))