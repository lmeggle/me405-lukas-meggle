'''
@file       PID.py
@brief      PID Controller
@details    This file contains the class PID, which can be used to create a PID controller for various tasks.
            
@page       page_PID PID Controller

@brief      This file contains the class PID, which can be used to create a PID controller for various tasks. Please see PID.py for the documentation.

@details

@section    sec_PID_ex Example usage

    PID = PID(1,2,3)
    while(True):
        print(PID.update(1.5,1))
        utime.sleep_ms(5)

@section    sec_PID_test Testing
            This Controller was created because of the flight stabilization system of the final project. Testing was only succesfully completed while moving a plane in the hands and watching the aileron reaction to counteracts the movement.
'''

import pyb
import utime
import time

## Implements a PID controller.
#
#  This class implements a PID Controller and contains methods to use it.
#  Details
#  @author Lukas Meggle
#  @date 05/13/20
class PID:
    
    ## Creates a PID Controller object.
    #
    #  @param kp A value to hold the desired proportional gain factor of the controller
    #  @param ki A value to hold the desired integration gain factor of the controller
    #  @param kd A value to hold the desired derivative gain factor of the controller
    def __init__ (self, kp, ki, kd):
        self.kp = kp;
        self.ki = ki;
        self.kd = kd;
        self.time_prior = utime.ticks_ms() - 10
        self.time_cur = utime.ticks_ms()
        self.error_prior = 0
        self.integral_prior = 0
        
    ## Method to run the PID controller and return a controller output.
    #
    #  Every time the this method is executed, the controller will calculate a output based on the gain factors and the inputs.
    #  @param soll The value of the desired setpoint.
    #  @param ist The input of the actual value of the system which should be controlled.
    def update(self, soll, ist):
        self.time_cur = utime.ticks_ms()
        iteration_time = time.ticks_diff(self.time_cur, self.time_prior)/1000;
        error = ist - soll;
        integral = self.integral_prior + (error * iteration_time);
        div = error - self.error_prior
        derivative = (div / iteration_time);
        self.error_prior = error;
        self.integral_prior = integral;
        output = self.kp*error + self.ki*integral + self.kd*derivative;
        self.time_prior = self.time_cur;
        return output
    
if __name__ == '__main__':
    PID = PID(1,2,3)
    while(True):
        print(PID.update(1.5,1))
        utime.sleep_ms(5)

