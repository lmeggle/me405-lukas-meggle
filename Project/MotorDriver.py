'''
@file       MotorDriver.py
@brief      MotorDriver
@details    This file contains the class MotorDriver with all the commands for enabling and spinning the motor and a test programm for executing some of those commands.
            
@page       page_MotorDriver Motor Driver

@brief      This file contains the class MotorDriver with all the commands for enabling and spinning the motor and a test programm for executing some of those commands. Please see MotorDriver.py which is part of the \ref motor package.

@details

@section    sec_Motor_Driver_ex Example usage

    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    tim = pyb.Timer(3, freq = 20000);
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    moe.enable()
    moe.set_duty(8)


@section    sec_Motor_Driver_test Testing
            The motor is able to spin in both directions with variable speeds. The succesful pull up or down command of GPIOs was tested with a multimeter.
'''
import pyb

## Implements methods for basic controlls of the motor.
#
#  This class implements a motor driver for the Nucleo Board.
#  Details
#  @author Lukas Meggle
#  @date 04/22/20
class MotorDriver:
    
    ## Creates a motor driver by initializing GPIO
    #  pins and turning the motor off for safety.
    #
    #  @param EN_pin A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer A pyb.Timer object to use for PWM generation on IN1_pin
    #  and IN2_pin.
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):        

        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin;
        self.IN2_pin = IN2_pin;
        self.Timer = timer;
        self.EN_pin.low()
        self.pwm_rev = self.Timer.channel(2, self.Timer.PWM, pin=self.IN2_pin)
        self.pwm_vor = self.Timer.channel(1, self.Timer.PWM, pin=self.IN1_pin)
        
    ## Method to enable the motor.
    #
    #  This function will pull the EN_pin to high.
    def enable (self):
        print ('Enabling Motor')
        self.EN_pin.high()

    ## Method to disable the motor.
    #
    #  This function will pull the EN_pin to low.
    def disable (self):
        print ('Disabling Motor')
        self.EN_pin.low()

    ## This method sets the duty cycle to be sent
    #  to the motor to the given level. Positive values
    #  cause effort in one direction, negative values
    #  in the opposite direction.
    #
    #  @param duty A signed integer holding the duty
    #  cycle of the PWM signal sent to the motor
    def set_duty (self, duty):
        if duty >= 0:
            self.pwm_vor.pulse_width_percent(0)
            self.pwm_rev.pulse_width_percent(duty)
        else:
            duty = -duty
            self.pwm_rev.pulse_width_percent(0)
            self.pwm_vor.pulse_width_percent(duty)

if __name__ == '__main__':

    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000);

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(10)