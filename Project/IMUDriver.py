'''
@file IMUDriver.py

@brief      IMU Driver Class
@details    This file contains the class i2c to control the Bosch BNO055 sensor.
            
@page       page_IMU IMU Sensor

@brief      The file IMUDriver.py holds the class i2c to implement the sensor into the system

@details


@section sec_intro Introduction
 This page documents the implementation of the Bosch BNO055 sensor.

 This gyro sensor is part of the board "DFRobot Gravity BNO055 + BMP280 intelligent 10DOF AHRS SKU SEN0253".

@section sec_first Connection
 The board is connected with +5V, ground, and an I2C Bus connection with the Pins SCL and SDA, in this case with the pins PB8 and PB9 of our Nucleo-Board.\n

@section sec_second Implementation of the class i2c
  Next up, a class was written to control the sensor (See: IMUDriver.py). This class contains the enabling and disabling of the sensor, setting the mode, and reading out calibration,
  euler angles and acceleration data. For a good understanding, it is recomended to be familiar with the datasheet of the BNO055:\n
  https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf\n

@section sec_final Demonstration of the sensor
  The sensor was tested by comparing the pysical orientation of the sensor with the euler angle output, the axial orientation of gravity on the acceleration output data
  and performing the calibration procedures shown in this video:\n
  https://youtu.be/Bw0WuAyGsnY?t=92\n
  The file IMUDIrver.py contains a script which prints out euler, acceleration and calibration data every 800ms.\n
  A quick demonstration is shown in this video:\n
  Note how the euler angles (in degress) changes realtive to the orientation of the sensor. Also, the gravitational force of 9.81 changes depending on the orientation of the board.

@section sec_IMU_ex Example usage

    adress = 0x28;
    bus = 1;
    IMU = i2c(bus, adress)
    IMU.enable()

    while(True):
        euler = IMU.get_euler()
        print('Euler Winkel in deg(x, y, z)')
        print(euler)


@author Lukas Meggle

@date 05/21/2020
'''

import pyb
import ustruct
import utime

## Implements methods for basic controlls of the sensor.
#
#  This class implements a i2c object for the Nucleo Board.
#  @author Lukas Meggle
#  @date 05/21/20
class i2c:
    
    ## Creates an i2c object to comunicate with the Bosch BNO055 Sensor via i2c.
    #
    #  The class initializes also several adresses for changing modes, power outputs or locating the registers of the sensor, which are used for the methods of this class.
    #  @param bus An integer value to hold the bus number of the i2c connection depending on the used pins.
    #  @param adress A Hexa-value to comunicate the i2c adress of the BNO055 sensor.
    def __init__ (self, bus, adress):
        #creating i2c object
        self.i2c = pyb.I2C(bus, pyb.I2C.MASTER)
        self.IMU_ADRESS = adress
        self.CALIB_STAT_ADR = 0X35
        #Mode adresses
        self.SET_OPMODE_ADR = 0X3D
        self.SET_PWRMODE_ADR = 0X3E
        #Power mode data
        self.PWRMODE_NORMAL = 0X00
        self.PWRMODE_LOWPOWER = 0X01
        self.PWRMODE_SUSPEND = 0X02
        #Operation mode data
        self.OPMODE_CONFIGMODE = 0X00
        self.OPMODE_ACCONLY = 0X01
        self.OPMODE_MAGONLY = 0X02
        self.OPMODE_GYROONLY = 0X03
        self.OPMODE_ACCMAG = 0X04
        self.OPMODE_ACCGYRO = 0X05
        self.OPMODE_MAGGYRO = 0X06
        self.OPMODE_AMG = 0X07
        self.OPMODE_IMU = 0X08
        self.OPMODE_COMPASS = 0X09
        self.OPMODE_M4G = 0X0A
        self.OPMODE_NDOF_FMC_OFF = 0X0B
        self.OPMODE_NDOF  = 0X0C
        
        #Data Locations
        self.DATA_EUL = 0X1A
        self.DATA_ACC = 0X08
        
        utime.sleep_ms(500)
    ## Method to enable the sensor.
    #
    #  This method will set the power mode to normal and set the operating mode to NDOF.
    def enable (self):
        self.config_mode()
        self.i2c.mem_write(self.PWRMODE_NORMAL, self.IMU_ADRESS, self.SET_PWRMODE_ADR)
        self.i2c.mem_write(self.OPMODE_NDOF, self.IMU_ADRESS, self.SET_OPMODE_ADR)
        utime.sleep_ms(10)

    ## Method to disable the sensor.
    #
    #  This function wset the power mode to SUSPEND and the operating mode to CONFIGMODE.
    def disable (self):
        print ('Disabling IMU')
        self.config_mode()
        self.i2c.mem_write(self.PWRMODE_SUSPEND, self.IMU_ADRESS, self.SET_PWRMODE_ADR)
    
    ## Method for setting the CONFIGMODE.
    #
    #  This method is replacable with the set_mode function, but was implemented for faster use.
    def config_mode(self):
        self.i2c.mem_write(self.OPMODE_CONFIGMODE, self.IMU_ADRESS, self.SET_OPMODE_ADR)
        utime.sleep_ms(30)
    ## Method for setting the operation mode of the sensor.
    #
    #  @param mode This value comunicates the desired operation mode.\n
    #  1: CONFIGMODE\n
    #  2: ACCONLY\n
    #  3: MAGONLY \n
    #  4: GYROONLY \n
    #  5: ACCMAG\n
    #  6: ACCGYRO\n
    #  7: MAGGYRO\n
    #  8: AMG\n
    #  9: IMU\n
    #  10: COMPASS\n
    #  11: M4G\n
    #  12: NDOF_FMC_OFF\n
    #  13: NDOF\n
    def set_mode(self, mode):
        m = mode;
        if m == 1:
            self.i2c.mem_write(self.OPMODE_CONFIGMODE, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to CONFIGMODE')
        elif m == 2:
            self.i2c.mem_write(self.OPMODE_ACCONLY, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to ACCONLY')
        elif m == 3:
            self.i2c.mem_write(self.OPMODE_MAGONLY, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to MAGONLY')
        elif m == 4:
            self.i2c.mem_write(self.OPMODE_GYROONLY, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to GYROONLY')
        elif m == 5:
            self.i2c.mem_write(self.OPMODE_ACCMAG, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to ACCMAG')
        elif m == 6:
            self.i2c.mem_write(self.OPMODE_ACCGYRO, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to ACCGYRO')
        elif m == 7:
            self.i2c.mem_write(self.OPMODE_MAGGYRO, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to MAGGYRO')
        elif m == 8:
            self.i2c.mem_write(self.OPMODE_AMG, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to AMG')
        elif m == 9:
            self.i2c.mem_write(self.OPMODE_IMU, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to IMU')
        elif m == 10:
            self.i2c.mem_write(self.OPMODE_COMPASS, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to COMPASS')
        elif m == 11:
            self.i2c.mem_write(self.OPMODE_M4G, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to M4G')
        elif m == 12:
            self.i2c.mem_write(self.OPMODE_NDOF_FMC_OFF, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to NDOF_FMC_OFF')
        elif m == 13:
            self.i2c.mem_write(self.OPMODE_NDOF, self.IMU_ADRESS, self.SET_OPMODE_ADR);
            print('Mode set to NDOF')
        else:
            print('Invalid mode number')      
    ## A Method which return a tulpe of euler angles.
    #
    #  This method will return a tulpe of the three current euler angles in [deg].
    def get_euler(self):
        data = self.i2c.mem_read(6, self.IMU_ADRESS, self.DATA_EUL)
        values = ustruct.unpack('<hhh',data)  
        return ([x/16 for x in values])
   
    ## A Method which return a tulpe of acceleration data.
    #
    #  This method will return a tulpe of the three current acceleration values in [m/s^2].
    def get_acc(self):
        data = self.i2c.mem_read(6, self.IMU_ADRESS, self.DATA_ACC)
        values = ustruct.unpack('<hhh',data)  
        return ([x/100 for x in values])
        
    ## A Method which return a tulpe calibration status.
    #
    #  This method will return a tulpe of the calibration status of (system, gyro, accelerometer, magnetometer).
    #  The output will be an integer between 0 and 3, while 3 means fully calibrated and 0 not calibrated.
    def get_calib_stat(self):
        data = self.i2c.mem_read(6, self.IMU_ADRESS, self.CALIB_STAT_ADR)
        sys_cal = (data[0] >> 6) & 0b11
        gyr_cal = (data[0] >> 4) & 0b11
        acc_cal = (data[0] >> 2) & 0b11
        mag_cal = data[0] & 0b11
        return (sys_cal, gyr_cal, acc_cal, mag_cal)
        
if __name__ == '__main__':

    # Creating the i2c object
    adress = 0x28;
    bus = 1;
    IMU = i2c(bus, adress)
    IMU.enable()
    
    #Printing out euler, acc, and calib data every 800ms
    while(True):
        euler = IMU.get_euler()
        print('Euler Winkel in deg(x, y, z)')
        print(euler)
        acc = IMU.get_acc()
        print('Acceleration in m/s^2 (x, y, z)')
        print(acc)
        calib = IMU.get_calib_stat()
        print('Calibration status (0[bad]-3[good]) (sys, gyr, acc, mag)')
        print(calib)
        utime.sleep_ms(800)
        print('\n')

