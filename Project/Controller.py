## @file Controller.py
#  This file contains the class Controller, which can be used to create a proportional
#  controller for various tasks.
#
#  @author Lukas Meggle
#  @date 05/13/20
'''
@file       Controller.py
@brief      Proportional Controller
@details    This file holds a class for creating a proportional controller object.
            
@page       page_Controller Controller

@brief      This file holds a class for creating a proportional controller object. See Controller.py for additional info.

@details

@section    sec_Controller_ex Example usage

    kp = float(input('Please enter kp: '))
    setpoint = 50
    current_value = enc.get_position()
    con = Controller(kp)
    controller_output = con.output(setpoint, current_value)


@section    sec_Encoder_test Testing
The Controller succesfully can control the motor position as seen in the source code from main_step.py..
'''

import pyb

## Implements a controller.
#
#  This class implements a proportional Controller and contains methods to use it.
#  Details
#  @author Lukas Meggle
#  @date 05/13/20
class Controller:
    
    ## Creates a Controller object.
    #
    #  @param kp A value to hold the desired proportional gain factor of the controller
    def __init__ (self, kp):
        self.kp = kp;   
        
    ## Method to run the controller and return a controller output.
    #
    #  Every time the this method is executed, the controller will calculate a output based on the proportional gain factors and the inputs.
    #  @param soll The value of the desired setpoint.
    #  @param ist The input of the actual value of the system which should be controlled.
    def update(self, soll, ist):
        error = ist - soll;
        output = error * self.kp;
        return output