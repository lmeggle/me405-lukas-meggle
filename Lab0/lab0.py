## @file lab0.py
#  Main function asking for the user input
#
#  This main function asks the user for the index and repeats itself until the user types in the letter 'e'
#  @author Your Name
#  @copyright License Info
#  @date 04/22/20

## Function which calculates the fibonacci number
#
#  The input of the function is the desired index
#  @return returns the fibonacci number
def fib (idx):
        print ('Calculating Fibonacci number at index n = {:}.'.format(idx))
        if idx==0: 
            return 0
        elif idx==1: 
            return 1
        else: 
            n0 = 0
            n1 = 1
            n = 1
            x = 0
            while n < idx:
                x = n0 + n1
                n0 = n1
                n1 = x
                n = n+1   
            return x


userinput = input('Please enter the Fibonacci index number: ')  
              
if __name__ == '__main__':
    while(True):
        if userinput.isdigit():
                index = int(userinput)
                print ('The Fibonacci number is: {:}'.format(fib(index)))
        elif userinput == 'e':
            break
        else:
            print("Invalid input! Please enter a positive number or (e) to exit")
        userinput = input('Enter another index, or (e) if you want to exit: ')